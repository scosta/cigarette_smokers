#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

enum ingrediente {kPapelTabaco, kPapelFosforo, kTabacoFosforo};

sem_t sem_papel_tabaco;
sem_t sem_papel_fosforo;
sem_t sem_tabaco_fosforo;
sem_t sem_consumo;

void delay() {
  time_t t = time(0) + 1;
  while (time(0) < t);
}

void* Agente(void* arg) {
  while (1) {
    int ingredientes = rand() % 3;
    if (ingredientes == kPapelTabaco) {
      printf("\n[Agente]: coloca papel e tabaco na mesa\n");
      delay();
      sem_post(&sem_papel_tabaco);
    } else if(ingredientes == kPapelFosforo) {
      printf("\n[Agente]: coloca papel e fosforo na mesa\n");
      delay();
      sem_post(&sem_papel_fosforo);
    } else if (ingredientes == kTabacoFosforo) {
      printf("\n[Agente]: coloca tabaco e fosforo na mesa\n");
      delay();
      sem_post(&sem_tabaco_fosforo);
    }
    sem_wait(&sem_consumo);
  }
}

void* FumantePapel(void* arg) {
  while(1) {
    sem_wait(&sem_tabaco_fosforo);
    printf("[Fumante Papel]: Consumido!\n");
    delay();
    sem_post(&sem_consumo);
  }
}

void* FumanteTabaco(void* arg) {
  while(1) {
    sem_wait(&sem_papel_fosforo);
    printf("[Fumante Tabaco]: Consumido!\n");
    delay();
    sem_post(&sem_consumo);
  }
}

void* FumanteFosforo(void* arg) {
  while(1) {
    sem_wait(&sem_papel_tabaco);
    printf("[Fumante Fosforo]: Consumido!\n");
    delay();
    sem_post(&sem_consumo);
  }
}


int main(int argc, char* arvg[]) {

  sem_init(&sem_papel_fosforo, 0, 0);
  sem_init(&sem_papel_tabaco, 0, 0);
  sem_init(&sem_tabaco_fosforo, 0, 0);
  sem_init(&sem_consumo, 0, 0);

  pthread_t tf_fosforo;
  pthread_t tf_papel;
  pthread_t tf_tabaco;
  pthread_t t_agente;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

  pthread_create(&tf_fosforo, &attr, FumanteFosforo, NULL);
  pthread_create(&tf_papel, &attr, FumantePapel, NULL);
  pthread_create(&tf_tabaco, &attr, FumanteTabaco, NULL);
  pthread_create(&t_agente, &attr, Agente, NULL);
  pthread_join(tf_fosforo, NULL);
  pthread_join(tf_papel, NULL);
  pthread_join(tf_tabaco, NULL);
  pthread_join(t_agente, NULL);

  return 0;
}
