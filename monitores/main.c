#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

enum ingrediente {kPapelTabaco, kPapelFosforo, kTabacoFosforo};

int tabaco_fosforo_count = 0;
int papel_fosforo_count  = 0;
int papel_tabaco_count   = 0;

pthread_mutex_t monitor_lock;
pthread_cond_t mesa_vazia;
pthread_cond_t f_fosforo_liberado;
pthread_cond_t f_papel_liberado;
pthread_cond_t f_tabaco_liberado;


void delay() {
  time_t t = time(0) + 1;
  while (time(0) < t);
}

void MonitorEsperaIngredientes(int ingredientes) {
  pthread_mutex_lock(&monitor_lock);
  if (ingredientes == kPapelTabaco) {
    while (papel_tabaco_count == 0) pthread_cond_wait(&f_fosforo_liberado,
                                                      &monitor_lock);
  } else if (ingredientes == kTabacoFosforo) {
    while (tabaco_fosforo_count == 0) pthread_cond_wait(&f_papel_liberado,
                                                        &monitor_lock);
  } else if (ingredientes == kPapelFosforo) {
    while (papel_fosforo_count == 0) pthread_cond_wait(&f_tabaco_liberado,
                                                       &monitor_lock);
  }
  pthread_mutex_unlock(&monitor_lock);
}

void MonitorConsomeIngredientes(int ingredientes) {
  pthread_mutex_lock(&monitor_lock);
  if (ingredientes == kPapelTabaco) papel_tabaco_count--;
  else if (ingredientes == kPapelFosforo) papel_fosforo_count--;
  else if (ingredientes == kTabacoFosforo) tabaco_fosforo_count--;
  pthread_cond_signal(&mesa_vazia);
  pthread_mutex_unlock(&monitor_lock);
}

void MonitorLiberaIngredientes(int ingredientes) {
  pthread_mutex_lock(&monitor_lock);
  while ((tabaco_fosforo_count + papel_fosforo_count + papel_tabaco_count) > 0) {
    pthread_cond_wait(&mesa_vazia,&monitor_lock);
  }
  if (ingredientes == kPapelTabaco) {
    papel_tabaco_count++;
    pthread_cond_signal(&f_fosforo_liberado);
  } else if (ingredientes == kPapelFosforo) {
    papel_fosforo_count++;
    pthread_cond_signal(&f_tabaco_liberado);
  } else if (ingredientes == kTabacoFosforo) {
    tabaco_fosforo_count++;
    pthread_cond_signal(&f_papel_liberado);
  }
  pthread_mutex_unlock(&monitor_lock);
}

void* Agente(void* arg) {
  while (1) {
    int ingredientes = rand() % 3;
    MonitorLiberaIngredientes(ingredientes);
    if (ingredientes == kPapelTabaco)         printf("\n[Agente]: Coloca Papel e Tabaco na mesa");
    else if (ingredientes == kPapelFosforo)   printf("\n[Agente]: Coloca Fosforo e Papel na mesa");
    else if (ingredientes == kTabacoFosforo)  printf("\n[Agente]: Coloca Tabaco e Fosforo na mesa");
  }
}

void* FumantePapel(void* arg){
  while(1) {
    MonitorEsperaIngredientes(kTabacoFosforo);
    printf("\n[Fumante Papel]: Consumido!\n");
    delay();
    MonitorConsomeIngredientes(kTabacoFosforo);
  }
}

void* FumanteTabaco(void* arg){
  while(1) {
    MonitorEsperaIngredientes(kPapelFosforo);
    printf("\n[Fumante Tabaco]: Consumido!\n");
    delay();
    MonitorConsomeIngredientes(kPapelFosforo);
  }
}

void* FumanteFosforo(void* arg){
  while(1) {
    MonitorEsperaIngredientes(kPapelTabaco);
    printf("\n[Fumante Fosforo]: Consumido!\n");
    delay();
    MonitorConsomeIngredientes(kPapelTabaco);
  }
}

int main(int argc, char* arvg[]){

  pthread_mutex_init(&monitor_lock, NULL);
  pthread_cond_init(&f_tabaco_liberado, NULL);
  pthread_cond_init(&f_fosforo_liberado, NULL);
  pthread_cond_init(&f_papel_liberado, NULL);
  pthread_cond_init(&mesa_vazia, NULL);

  pthread_t tf_fosforo;
  pthread_t tf_papel;
  pthread_t tf_tabaco;
  pthread_t t_agente;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

  pthread_create(&tf_fosforo, &attr, FumanteFosforo, NULL);
  pthread_create(&tf_papel, &attr, FumantePapel, NULL);
  pthread_create(&tf_tabaco, &attr, FumanteTabaco, NULL);
  pthread_create(&t_agente, &attr, Agente, NULL);
  pthread_join(tf_fosforo, NULL);
  pthread_join(tf_papel, NULL);
  pthread_join(tf_tabaco, NULL);
  pthread_join(t_agente, NULL);

  return 0;
}
